variable "resource_group_name" {
   description = "Name of the resource group in which the resources will be created"
   default     = "skb_iac_sclset"
}

variable "prefix" {
    default     = "skb-iac-sclset"
    description = "The prefix which should be used for all resources in this example"
}

variable "location" {
    default = "germanywestcentral"
    description = "The Azure Region in which all resources in this example should be created."
}

/*
variable "location" {
   default = "germanywestcentral"
   description = "Location where resources will be created"
}

variable "instances_count" {
    default = 2
    description = "instances count"
}

variable "vmss_name" {
    default = "vmscaleset"
}

variable "tags" {
   description = "Map of the tags to use for the resources that are deployed"
   type        = map(string)
   default = {
      environment = "skb codelab"
   }
}

variable "application_port" {
   description = "Port that you want to expose to the external load balancer"
   default     = 80
}

variable "admin_user" {
   description = "User name to use as the admin account on the VMs that will be part of the VM scale set"
   default     = "azureuser"
}

variable "admin_password" {
   description = "Default password for admin account"
}
*/